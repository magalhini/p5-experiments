let segments = [];
let base = {};
const segmentLength = 100;
const RADIUS = 8;
let angleASlider;
let angleBSlider;
let isMouseDrag = false;

function setup() {
  createCanvas(600, 400);
  angleASlider = document.getElementById('angleA');
  angleBSlider = document.getElementById('angleB');

  segments[0] = new Segment(null, { x: 300, y: 200 }, segmentLength, 40);

  for (let i = 1; i < 2; i++) {
    segments[i] = new Segment(segments[i - 1], null, segmentLength, 40);
  }

  base = createVector(width / 2, height / 2);
}

function checkBoundaries(x, y, w, h) {
  return {
    x: (x < w) ? x : w,
    y: (y > h && y > 0) ? y : h
  };
}

function mouseDragged(evt) {
  const { screenX, screenY } = evt;
  const end = segments[segments.length - 1];

  if (dist(mouseX, mouseY, end.b.x, end.b.y) < RADIUS * 2) {
    isMouseDrag = true;
  }
}

function mouseReleased() {
  isMouseDrag = false;
}

function draw() {
  background(51);

  const totalNumber = segments.length;
  const end = segments[segments.length - 1];
  const ax = parseInt(angleASlider.value, 10);
  const ay = parseInt(angleBSlider.value, 10);

  if (isMouseDrag) {
    end.follow(checkBoundaries(mouseX, mouseY, width, length));
  }

  for (let i = totalNumber - 2; i >= 0; i--) {
    segments[i].followChild(segments[i + 1], segments[i + 1]);
    segments[i].update();
  }

  segments[0].setVectorA(base);

  for (let i = 1; i < totalNumber; i++) {
    segments[i].setVectorA(segments[i - 1].b);
  }

  for (let i = 0; i < totalNumber; i++) {
    segments[i].display();
  }

  fill(70,40,50);
  strokeWeight(2);
  ellipse(base.x, base.y, 18, 18);
  fill(50,50,50);
  ellipse(end.a.x, end.a.y, RADIUS * 2, RADIUS * 2);
  fill(250,50,50);
  stroke(0);
  ellipse(end.b.x, end.b.y, RADIUS * 3, RADIUS * 3);
}

console.log('Hello.');
