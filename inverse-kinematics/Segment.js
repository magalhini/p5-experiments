class Segment {
  constructor(parent = null, location, len, angle) {
    this.angle = angle;
    this.length = len;
    this.a = !parent ? createVector(location.x, location.y) : parent.b.copy();
    this.b = createVector();
    this.calculateB();
  }

  setVectorA(baseVector) {
    this.a = baseVector.copy();
    this.calculateB();
  }

  calculateB() {
    // Polar to Cartesian transformation
    const dx = this.length * Math.cos(this.angle);
    const dy = this.length * Math.sin(this.angle);
    this.b.set(this.a.x + dx, this.a.y + dy);
  }

  followChild(child) {
    this.follow({
      x: child.a.x,
      y: child.a.y
    });
  }

  follow(location) {
    const target = createVector(location.x, location.y);
    const direction = p5.Vector.sub(target, this.a);
    this.angle = direction.heading();

    direction.setMag(this.length);
    direction.mult(-1);

    this.a = p5.Vector.add(target, direction);
  }

  update() {
    this.calculateB();
  }

  display(i) {
    stroke(255);
    strokeWeight(5);
    line(this.a.x, this.a.y, this.b.x, this.b.y);
  }
}
